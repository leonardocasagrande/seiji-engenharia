$(document).ready(function () {
  $(".hamburger").click(function () {
    if ($(".hamburger").hasClass("is-active")) {
      $(this).removeClass("is-active");
    } else {
      $(this).addClass("is-active");
    }
  });
});

$(".ba-parent, .arrow-click").click(function (e) {
  e.preventDefault();
  let foco = $("#foco")[0].offsetTop;
  window.scrollTo(0, foco - 102);
});

$(window).scroll(function () {
  if (window.scrollY > 200) {
    $("nav").addClass("bg-red");
  } else {
    $("nav").removeClass("bg-red");
  }
});

let caminho = window.location.pathname;

if (caminho === "/seiji/quem-somos/") {
  $(".menu-1").addClass("menu-active");
} else if (caminho === "/seiji/equipe/") {
  $(".menu-2").addClass("menu-active");
} else if (caminho === "/seiji/projetos/") {
  $(".menu-3").addClass("menu-active");
} else if (caminho === "/seiji/fale-conosco/") {
  $(".menu-4").addClass("menu-active");
} else {
  $(".nav-item ").removeClass("menu-active");
}

function createCarousel(container, controls) {
  var slider = tns({
    container: container,
    items: 1,
    slideBy: "1",
    autoplay: true,
    controlsContainer: controls,
    nav: false,
    loop: true,
    autoplayButtonOutput: false,
  });
}

 if (caminho === "/seiji/equipe/") {
  createCarousel(".equipe-container", ".equipe-nav");
} else if (caminho === "/seiji/quem-somos/") {
  createCarousel(".valores-container", ".valores-nav");
}
console.log(caminho);

//MASCARAS
$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".email").attr("onblur", "validateEmail(this)");

$(document).ready(function () {
  if (!document.cookie) {
    $(".cookies").addClass("show");

    $(".btn-success").click(function (e) {
      e.preventDefault();
      document.cookie = "cookies=accepted";
      $(".cookies").removeClass("show");
    });
  }
});

$(".veja-mais").click(function (e) {

  e.preventDefault();
  $(".container-foto").toggleClass("vendo-mais");
  $('.veja-mais').addClass('d-none');

  if (window.screen.width < 400) {
    window.scrollTo(600,1300);
  } else {
    window.scrollTo(1500,2200);
  }
});
