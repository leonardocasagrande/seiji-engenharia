<footer class="ref-lg ">
	<div class="container-custom justify-content-lg-center ">
		<div class="menu-footer align-items-lg-center d-none d-lg-flex">
			<a href="<?= get_site_url(); ?>/quem-somos">quem somos</a>
			<a href="<?= get_site_url(); ?>/equipe">equipe</a>
			<a href="<?= get_site_url(); ?>/projetos">projetos</a>
			<a href="<?= get_site_url(); ?>/fale-conosco">fale conosco</a>
		</div>

		<div class="lg-size d-lg-none">
			<a class="d-none " href="https://www.google.com.br/maps/dir//R.+Joaquim+Guilherme+da+Costa,+575+-+Parque+Ortol%C3%A2ndia,+Hortol%C3%A2ndia+-+SP,+13184-070/@-22.8711638,-47.2264937,21z/data=!4m9!4m8!1m0!1m5!1m1!1s0x94c8bbf654b11e1b:0xb76099ca5a82aaf6!2m2!1d-47.2264635!2d-22.871141!3e0" target="_blank"><span class="endereco">R. Joaquim Guilherme</br> da Costa, 575, Sala 04,</br> Pq. Ortolândia,</br>Hortolândia - SP</span></a>

			<a class="d-none " href="tel:+551938281970"><span>19<span class="maior">3828-1970</span></span></a>




			<a class="d-none " href="mailto:contato@seijiengenharia.com.br"><span>contato@seijiengenharia.com.br</span></a>

			<div class="midias justify-content-center justify-content-lg-start py-3">
				<a href="https://instagram.com/seijiengenharia?igshid=o6c8oj3lsdp8"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.svg" alt=""></a>

				<!-- <a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/twitter.svg" alt=""></a> -->

				<!-- <a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.svg" alt=""></a> -->
			</div>
		</div>
	</div>


	<span class="humann justify-content-center justify-content-lg-end">Desenvolvido por <a href="https://www.humann.com.br" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/humann.png" alt=""></a></span>

	<div class="detalhes-bg">
		<img class="d-lg-none d-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bg-footer-1.svg" alt="abstract">
		<img class="d-lg-none d-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bg-footer-2.svg" alt="abstract">

		<img class="d-none img-1 detail-lg" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/footer-lg-1.png" alt="triangulo">


	</div>


</footer>

<!-- Principal JavaScript do Bootstrap -->
<?php wp_footer(); ?>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.js" integrity="sha512-UHlZzRsMRK/ENyZqAJSmp/EwG8K/1X/SzErVgPc0c9pFyhUwUQmoKeEvv9X0uNw8x46FxgIJqlD2opSoH5fjug==" crossorigin="anonymous"></script>
<script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
</body>

</html>