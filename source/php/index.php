<?php get_header() ?>

<section class="banner ">
    <div class="container-img bg-home">


    </div>
</section>

<section class="banner-info container">
    <h1 class="title">resultados </br>sólidos</h1>
    <span class="sub-title d-none d-lg-block">Estratégias inovadoras </br>na construção</span>

    <a href="<?= get_site_url(); ?>/projetos" class="btn-cta cta-banner">conheça +</a>

    <div class="ba-parent">
        <a class="down-arrow" href="">
            <div class="banner-arrow"></div>
        </a>
    </div>
</section>

<section id="foco" class="projetos-home ">
    <img class="d-none d-lg-block linha-detalhe" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bg-projetos-desk.png" alt="">

    <?= get_template_part('projetos'); ?>

    <div class="d-lg-none">
        <img class="img-2-home" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/projeto-home-mob.png" alt="">

        <div class="detail"></div>

        <div class="sobre-home">

            <span class="text">A Seiji Engenharia e Construções é uma empresa que busca sempre a inovação para melhorar
                o atendimento e simplificar os processos.</span>

            <a href="<?php echo get_site_url(); ?>/quem-somos/" class="btn-cta">conheça +</a>

        </div>
    </div>
</section>

<section class="cta-box custom-bg py-5 d-none d-lg-block">
    <a href="<?= get_site_url(); ?>/projetos" class="btn-cta">conheça nossos projetos</a>
</section>

<section class="historia-home d-lg-block d-none">

    <div class="custom-box-right">
        <div class="px-0">
            <p>A Seiji Engenharia e Construções é uma empresa
                que busca sempre a inovação para melhorar
                o atendimento e simplificar os processos.
                Especializada em construções personalizadas e de
                alto padrão, vem consolidando sua marca no
                mercado ganhando a confiança de seus clientes
                com competência e total transparência.</p>
            <a href="<?= get_site_url(); ?>/quem-somos" class="btn-cta d-none d-lg-flex">+</a>
        </div>
        <img class="col-6 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/historia-home.png" alt="">
    </div>

</section>


<div class="contato-component ">

    <div class="contato-box custom-contato-home">

        <img class='detail-1 ' src="<?= get_stylesheet_directory_uri(); ?>/dist/img/contato-home-d1.png" alt="">

        <img class='detail-2 d-none d-lg-block' src="<?= get_stylesheet_directory_uri(); ?>/dist/img/contato-home-d2.png" alt="">



        <span class="title">contato</span>

        <div class="form col-lg-7 margin-auto">

            <?= do_shortcode('[contact-form-7 id="51" title="Contato"]'); ?>
            <!-- <input type="text" placeholder="nome">
            <input type="text" placeholder="e-mail">
            <div class="grid">
                <input type="text" placeholder="telefone">
                <input type="text" placeholder="assunto">
            </div>
            <textarea placeholder="mensagem" name="" id="" cols="30" rows="10"></textarea>

            <a href="#" class="btn-cta">Enviar</a> -->
        </div>

    </div>

</div>


<!-- <div class="cookies">

    <div class="cookie-container">
    </div>

    <div class="cookie-warn">
        <div class="cookie-text">Nós usamos cookies e outras tecnologias semelhantes para melhorar a sua experiência em nossos serviços, oferecer a você uma experiência mais responsiva e personalizada de navegação. Ao fazer uso desta plataforma, você concorda com a utilização dos mesmos. Para saber mais, acesse nossa <a href="https://humann.com.br/seiji/wp-content/themes/seiji-engenharia/dist/politica-de-privacidade.pdf" target="_blank">Política de privacidade</a>.</div>

        <div class="buttons d-flex h-50 flex-column flex-lg-row">
            <a href="#" class="btn btn-success btn-sm">Aceito</a>
        </div>
    </div>
</div> -->



<?= get_template_part('cta-box'); ?>


<?php get_footer() ?>