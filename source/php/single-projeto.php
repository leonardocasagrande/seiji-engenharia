<?php get_header();
the_post(); ?>


<section class="banner margin-discount">
    <div class="container-img overflow-hidden">
        <div class="img-banner" style="background: url(<?= the_field('imagem'); ?>) center center no-repeat ; background-size:cover "></div>
        <div class="diagonal "></div>
    </div>
</section>

<section class="banner-info container single-lg-header single-banner">

    <span class="intro ">projeto</span>
    <h1 class="title"><?= the_title(); ?></h1>

    <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
    <!-- <span class="area">Área do Terreno: <?= the_field('terreno'); ?> m²</span> -->
    <span class="area">Área Construída: <?= the_field('construcao'); ?> m²</span>

    <!-- <span class="area d-lg-block d-none pt-5">"<?= the_field('resumo'); ?>"</span> -->

    <div class="arrow-click btn-cta">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-down.png" alt="seta para baixo">
    </div>

</section>

<section id="foco" class="galeria-single">
    <div class="container-foto">
        <?php
        $images = get_field('galeria');
        if ($images) : ?>
            <?php foreach ($images as $image) : ?>


                <a href="<?= $image; ?>" data-lightbox="roadtrip">
                    <div class="image" style="background: url(<?= $image; ?>)center center no-repeat; background-size: cover"></div>
                </a>

            <?php endforeach; ?>
        <?php endif; ?>
    </div>


    <a href="#" class="btn-cta veja-mais">Ver todas</a>


</section>

<section class="contato-quem">
    <img class="detail-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob.svg" alt="">

    <img class="detail-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob-2.svg" alt="">

    <?= get_template_part('contato-component'); ?>
</section>


<?php get_footer(); ?>