<?php get_header(); ?>

<section class="banner ">
    <div class="container-img bg-projetos">
    </div>
</section>

<section class="banner-info margin-correction-2 container ">
    <h1 class="title">Projetos</h1>

    <h2 class="sub d-lg-none">Especialistas em construção de alto padrão</h2>


    <div class="arrow-click btn-cta d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-down.png" alt="">
    </div>

</section>


<section id="foco" class="d-lg-none">

    <div class="slider-home">
        <div class="casa-slider">

            <?php

            $argsProjetos = array(
                'post_type' => 'projeto',
                'orderby' => 'date'
            );
            $Projetos = new WP_Query($argsProjetos);
            //   var_dump($postAtual);
            if ($Projetos->have_posts()) :
                while ($Projetos->have_posts()) : $Projetos->the_post();
            ?>




                    <div class="casa-item">
                        <div class="imagem" style="background: url(<?= the_field('imagem'); ?>) center center no-repeat">
                            <div class="filtro-amarelo"></div>
                            <span class="nome"><?= the_title(); ?></span>
                            <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->

                            <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>
                        </div>
                    </div>

            <?php endwhile;
            endif; ?>




        </div>

        <div class="casa-nav">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowl.svg" alt="">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowr.svg" alt="">
        </div>
    </div>

</section>

<section class="contato-quem d-lg-none">
    <img class="detail-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob.svg" alt="">

    <img class="detail-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob-2.svg" alt="">

    <?= get_template_part('contato-component'); ?>
</section>


<section class="projetos-lg d-none d-lg-block">

    <div class="header-section">
        <!-- <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detalhe-projeto-lg-intro.png" alt=""> -->
        <span class="title">base sólida e</br> pilares consistentes</span>
        <span class="sub">Especialistas em construções de alto padrão.</span>
    </div>


    <div class="d-none d-lg-flex flex-wrap desk-grid">
        <?php
        $contador = 1;
        $argsProjetos = array(
            'post_type' => 'projeto',
            'posts_per_page' => '-1'
        );
        $projetos = new WP_Query($argsProjetos);
        if ($projetos->have_posts()) : while ($projetos->have_posts()) : $projetos->the_post();
        ?>
                <?php if ($contador == 1) : ?>




                    <div class="bg-grid hover-1 col-lg-6" style="background: url(<?= the_field('imagem'); ?>);">
                        <div class="filtro-amarelo "></div>


                        <div class="text">
                            <span class="title"><?= the_title(); ?> </span>
                            <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                            <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                            <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                            <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                        </div>
                    </div>





                <?php elseif ($contador == 2) : ?>



                    <div class="bg-grid  col-lg-6">
                        <div class="bg-grid hover-2 col-lg-12 h-50" style="background: url(<?= the_field('imagem'); ?>);">
                            <div class="filtro-amarelo "></div>

                            <div class="text">
                                <span class="title"><?= the_title(); ?> </span>
                                <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                                <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                                <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                                <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                            </div>
                        </div>




                        <div class="bg-grid  d-flex h-50">





                        <?php elseif ($contador == 3 || $contador == 4) : ?>





                            <div class="bg-grid hover-3 col-lg-6" style="background: url(<?= the_field('imagem'); ?>);">
                                <div class="filtro-amarelo "></div>

                                <div class="text">
                                    <span class="title"><?= the_title(); ?> </span>
                                    <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                                    <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                                    <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                                    <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                                </div>
                            </div>





                        <?php elseif ($contador == 5) : ?>
                        </div>
                    </div>
                    <div class="bg-grid  col-lg-6">
                        <div class="bg-grid hover-2 col-lg-12 h-50" style="background: url(<?= the_field('imagem'); ?>);">
                            <div class="filtro-amarelo "></div>

                            <div class="text">
                                <span class="title"><?= the_title(); ?> </span>
                                <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                                <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                                <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                                <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                            </div>
                        </div>




                        <div class="bg-grid  d-flex h-50">


                        <?php elseif ($contador == 6 || $contador == 7) : ?>

                            <div class="bg-grid hover-3 col-lg-6" style="background: url(<?= the_field('imagem'); ?>);">
                                <div class="filtro-amarelo "></div>

                                <div class="text">
                                    <span class="title"><?= the_title(); ?> </span>
                                    <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                                    <span class="arquitetura">Arquitetura:<?= the_field('arquitetura'); ?></span>
                                    <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                                    <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                                </div>
                            </div>
                        <?php elseif ($contador == 8) : ?>
                        </div>
                    </div>
                    <div class="bg-grid hover-1 col-6" style="background: url(<?= the_field('imagem'); ?>);">
                        <div class="filtro-amarelo "></div>


                        <div class="text">
                            <span class="title"><?= the_title(); ?> </span>
                            <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                            <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                            <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                            <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                        </div>
                    </div>
                <?php elseif ($contador == 9) : $contador = 0; ?>
                    <div class="bg-grid hover-1 col-12" style="background: url(<?= the_field('imagem'); ?>);">
                        <div class="filtro-amarelo "></div>


                        <div class="text">
                            <span class="title"><?= the_title(); ?> </span>
                            <!-- <span class="situacao"><?= the_field('situacao'); ?></span> -->
                            <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span>
                            <span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span>

                            <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a>

                        </div>
                    </div>

                <?php endif; ?>

        <?php $contador++;
            endwhile;
        endif; ?>

    </div>


</section>


<section class="cta-box cta-quem py-5 d-none d-lg-block position-relative">
    <a href="<?= get_site_url(); ?>/fale-conosco" class="btn-cta font-size-24">entre em contato</a>
</section>


<?php get_footer(); ?>