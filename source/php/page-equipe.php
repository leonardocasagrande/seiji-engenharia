<?php get_header(); ?>

<section class="banner ">
    <div class="container-img bg-equipe-lg  ">
    </div>
</section>

<section class="banner-info equipe-intro container equipe-custom">
    <h1 class="title position-top-lg ">Equipe</h1>

    <h2 class="sub d-lg-none">Somos uma empresa que busca sempre a inovação para melhorar o atendimento e simplificar os processos. Especializada em construções personalizadas e de alto padrão, vem consolidando sua marca no mercado ganhando a confiança de seus clientes com competência e total transparência. Nossa meta vai além de entregar um produto de ótima qualidade para nossos clientes.</h2>

    <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-equipe-mob-1.svg" alt="">

</section>

<section class="equipe d-lg-none">

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detalhe-equipe-mob.svg" alt="" class="detail-equipe">

    <div class="wrapper">

        <div class="equipe-container">

            <div class="pessoa">

                <!-- <img class="detalhe-seiji" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-equipe-mob-1.svg" alt=""> -->

                <img style="width: 100%;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/equipe-1.png" alt="Gustavo Seiji">

                <span class="name">Engº Gustavo Seiji</span>
                <span class="caption">Sócio proprietário</span>

            </div>

            <div class="pessoa">

                <img class="detalhe-jessica" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-equipe-mob-2.svg" alt="">

                <img style="width: 315px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/equipe-2.png" alt="Leticia">

                <span class="name">Engª Letícia Oliveira</span>
                <span class="caption">Sócio proprietário</span>

            </div>

            <div class="pessoa">

                <img class="detalhe-jessica" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/d-equipe-mob-2.svg" alt="">

                <img style="width: 315px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/ivanildo.png" alt="Ivanildo">

                <span class="name">Engº Ivanildo Sellis</span>
                <span class="caption">Sócio proprietário</span>

            </div>


            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng1.png" alt="Diego">
                <span class="name">Engº Diego Souza</span>
                <span class="funcao">Obras</span>

            </div>

            <!-- <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng2.png" alt="Raphael">
                <span class="name">Engº Raphael Zampieri </span>
                <span class="funcao">Obras</span>

            </div> -->

            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng3.png" alt="Bruno">
                <span class="name">Engº Bruno de Souza </span>
                <span class="funcao">Obras</span>

            </div>


            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng4.png" alt="Erika">
                <span class="name">Engª Erika Souza</span>
                <span class="funcao">Suprimentos</span>

            </div>



            <div style="position: relative; top: -7px;" class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng5.png" alt="Gabriela">
                <span class="name gabriela">Gabriela Barbosa</span>
                <span class="funcao">RH e administrativo</span>

            </div>

            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng6.png" alt="Wallison">
                <span class="name">Engº Wallison de Oliveira </span>
                <span class="funcao">Orçamentos</span>

            </div>

            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/carlos.png" alt="Carlos Campos">
                <span class="name">Engº Carlos Campos</span>
                <span class="funcao">Obras</span>

            </div>

            <!-- <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/geovanni.png" alt="Geovanni">
                <span class="name">Engº Geovanni Chagas</span>
                <span class="funcao">Obras</span>

            </div> -->

            <!-- <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/leonardo.png" alt="Leonardo">
                <span class="name">Engº Leonardo Cosequi</span>
                <span class="funcao">Obras</span>

            </div> -->

            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mateus-viana.png" alt="Mateus">
                <span class="name">Engº Mateus Viana</span>
                <span class="funcao">Obras</span>
            </div>

            <div class="pessoa p-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/natalia.png" alt="Natalia">
                <span class="name">Engª Natalia Coutinho</span>
                <span class="funcao">Suprimentos</span>
            </div>



        </div>

    </div>

    <div class="equipe-nav">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/left.svg" alt="left">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/right.svg" alt="right">
    </div>

</section>

<section class="equipe-lg  d-none d-lg-block">

    <div class="container">
        <p>Somos uma empresa que busca sempre a inovação para melhorar o atendimento e simplificar os processos. Especializada em construções personalizadas e de alto padrão, vem consolidando sua marca no mercado ganhando a confiança de seus clientes com competência e total transparência. </p>





    </div>

    <div class="lideres container">
        <div class="item">
            <img style="width: 445px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/equipe-1.png" alt="Gustavo">
            <span class="name">Engº Gustavo Seiji</span>
            <span class="funcao">Sócio proprietário</span>
        </div>

        <div class="item">
            <img style="width: 445px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/ivanildo.png" alt="Ivanildo Sellis">
            <span class="name">Engº Ivanildo Sellis</span>
            <span class="funcao">Sócio proprietário</span>
        </div>

        <div class="item">
            <img style="width: 445px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/equipe-2.png" alt="Leticia">
            <span class="name">Engª Letícia Oliveira</span>
            <span class="funcao">Sócio proprietário</span>
        </div>


    </div>

    <div class="engenheiros container">

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng1.png" alt="Diego">
            <span class="name">Engº Diego Souza</span>
            <span class="funcao">Obras</span>

        </div>

        <!-- <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng2.png" alt="Raphael">
            <span class="name">Engº Raphael Zampieri </span>
            <span class="funcao">Obras</span>

        </div> -->

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng3.png" alt="Bruno">
            <span class="name">Engº Bruno de Souza </span>
            <span class="funcao">Obras</span>

        </div>

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/carlos.png" alt="Carlos Campos">
            <span class="name">Engº Carlos Campos</span>
            <span class="funcao">Obras</span>

        </div>

        <!-- <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/geovanni.png" alt="Geovanni Chagas">
            <span class="name">Engº Geovanni Chagas</span>
            <span class="funcao">Obras</span>

        </div> -->

        <!-- <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/leonardo.png" alt="Leonardo Cosequi">
            <span class="name">Engº Leonardo Cosequi</span>
            <span class="funcao">Obras</span>

        </div> -->

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng4.png" alt="Erika">
            <span class="name">Engª Erika Souza</span>
            <span class="funcao">Suprimentos</span>

        </div>



        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng5.png" alt="Gabriela">
            <span class="name gabriela">Gabriela Barbosa</span>
            <span class="funcao">RH e administrativo</span>

        </div>

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eng6.png" alt="Wallison">
            <span class="name">Engº Wallison de Oliveira </span>
            <span class="funcao">Orçamentos</span>

        </div>

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mateus-viana.png" alt="Mateus">
            <span class="name">Engº Mateus Viana</span>
            <span class="funcao">Obras</span>
        </div>

        <div class="item">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/natalia.png" alt="Natalia">
            <span class="name">Engª Natalia Coutinho</span>
            <span class="funcao">Suprimentos</span>
        </div>



    </div>

</section>


<section class="projetos-equipe position-relative">

    <?= get_template_part('projetos'); ?>

</section>


<section class="contato-quem d-lg-none">
    <img class="detail-1" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob.svg" alt="detalhe">

    <img class="detail-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-contato-quem-mob-2.svg" alt="detalhe 2">

    <?= get_template_part('contato-component'); ?>
</section>

<section class="cta-box cta-equipe py-5 d-none d-lg-block position-relative">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/equipe-detalhe-cta.png" alt="detalhe 3">
    <a href="<?= get_site_url(); ?>/projetos" class="btn-cta">conheça nossos projetos</a>
</section>

<?= get_template_part('cta-box'); ?>

<?php get_footer(); ?>