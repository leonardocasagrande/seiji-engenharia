<!DOCTYPE html>

<html lang="pt_BR">

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-77C43XKKFM"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-77C43XKKFM');
	</script>


	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>

		Seiji Engenharia <?php wp_title(''); ?>

	</title>

	<meta name="robots" content="index, follow" />

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Barlow&display=swap" rel="stylesheet">

	<!-- <link rel="preload" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css" as="style" onload="this.onload=null;this.rel='stylesheet'"> -->

	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css" integrity="sha512-Woz+DqWYJ51bpVk5Fv0yES/edIMXjj3Ynda+KWTIkGoynAMHrqTcDUQltbipuiaD5ymEo9520lyoVOo9jCQOCA==" crossorigin="anonymous" />

	<link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">



	<?php wp_head(); ?>
</head>

<body>

	<header class="menu-fixo">


		<nav class="navbar  p-0 navbar-expand-lg  ">
			<div class=" container p-lg-0 d-flex justify-content-between w-100 align-items-center ">
				<a href="<?php echo get_site_url(); ?>/">
					<div class="logo-site ml-md-0"> </div>
				</a>



				<div class="collapse  navbar-collapse-sm  " id="navbarNavDropdown">
					<div class="container menu-lg px-lg-0">

						<ul class="navbar-nav  px-lg-0 ">

							<li class="nav-item menu-1">
								<a class="nav-link " href="<?php echo get_site_url() ?>/quem-somos">quem somos
									<img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/menu-detail.png" alt="">
								</a>
							</li>

							<li class="nav-item menu-2">
								<a class="nav-link " href="<?php echo get_site_url(); ?>/equipe">equipe
									<img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/menu-detail.png" alt="">
								</a>

							</li>

							<li class="nav-item menu-3">
								<a class="nav-link  " href="<?php echo get_site_url(); ?>/projetos"> projetos
									<img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/menu-detail.png" alt="">
								</a>

							</li>

							<li class="nav-item menu-4">
								<a class="nav-link " href="<?php echo get_site_url(); ?>/fale-conosco">fale conosco
									<img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/menu-detail.png" alt="">
								</a>

							</li>

						</ul>

						<div class="d-lg-flex align-items-center ">
							<span class="pr-lg-4"><a href="tel:+551938281970"><i class="fas fa-phone-volume pr-2"></i> 19<span class="maior">3828-1970</span></a></span>

							<span class="mr-3"><a href="https://api.whatsapp.com/send?phone=5519998807714"><i class="fab fa-whatsapp pr-1"></i> 19<span class="maior">99880-7714</span></a></span>

							<span><a href="mailto:contato@seijiengenharia.com.br">contato@seijiengenharia.com.br</a></span>
						</div>

						<div class="midias">
							<a href="https://instagram.com/seijiengenharia"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.svg" alt=""></a>

							<!-- <a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/twitter.svg" alt=""></a> -->

							<!-- <a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.svg" alt=""></a> -->
						</div>
					</div>
				</div>


				<button class="hamburger" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" id="hamburger-1">
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
				</button>

				<!-- <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				</button> -->

			</div>

		</nav>


	</header>