<?= get_header(); ?> <section class="banner"><div class="container-img bg-home"></div></section><section class="banner-info container"><h1 class="title">resultados<br>sólidos</h1><span class="sub-title d-none d-lg-block">Estratégias inovadoras<br>na construção</span> <a href="<?= get_site_url(); ?>/projetos" class="btn-cta cta-banner">conheça +</a><div class="ba-parent"><a class="down-arrow" href=""><div class="banner-arrow"></div></a></div></section><section class="container py-5"><span class="title-politica"><?= the_title(); ?></span> <?= the_content(); ?> </section><style>.title-politica {
    padding-bottom: 15px;
    font-weight: bold;
    font-size: 30px;

  }</style> <?= get_footer(); ?>