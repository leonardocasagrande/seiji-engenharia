<div><div class="bg-teste d-lg-none"></div><div class="banner-projetos"><span class="title">projetos</span></div><div class="slider-home d-lg-none"><div class="casa-slider"> <?php

            $argsProjetos = array(
                'post_type' => 'projeto',
                'orderby' => 'date'
            );
            $Projetos = new WP_Query($argsProjetos);
            //   var_dump($postAtual);
            if ($Projetos->have_posts()) : while ($Projetos->have_posts()) : $Projetos->the_post();  ?> <div class="casa-item"><div class="imagem" style="background: url(<?= the_field('imagem'); ?>) center center no-repeat"><div class="filtro-amarelo"></div><span class="nome"><?= the_title(); ?></span><!-- <span class="situacao"><?= the_field('situacao'); ?></span> --> <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a></div></div> <?php endwhile;
            endif; ?> </div><div class="casa-nav"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowl.svg" alt=""> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowr.svg" alt=""></div></div><div class=""><div class="d-none d-lg-flex desk-grid"> <?php
            $contador = 1;
            $argsProjetos = array(
                'post_type' => 'projeto',
                'posts_per_page' => '4'
            );
            $projetos = new WP_Query($argsProjetos);
            if ($projetos->have_posts()) : while ($projetos->have_posts()) : $projetos->the_post();
            ?> <?php if ($contador == 1) : ?> <div class="bg-grid hover-1 col-lg-6" style="background: url(<?= the_field('imagem'); ?>);"><div class="filtro-amarelo"></div><div class="text"><span class="title"><?= the_title(); ?> </span><!-- <span class="situacao"><?= the_field('situacao'); ?></span> --> <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span><span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span> <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a></div></div> <?php elseif ($contador == 2) : ?> <div class="bg-grid col-lg-6"><div class="bg-grid hover-2 col-lg-12 h-50" style="background: url(<?= the_field('imagem'); ?>);"><div class="filtro-amarelo"></div><div class="text"><span class="title"><?= the_title(); ?> </span><!-- <span class="situacao"><?= the_field('situacao'); ?></span> --> <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span><span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span> <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a></div></div><div class="bg-grid d-flex h-50"> <?php elseif ($contador >= 3) : ?> <div class="bg-grid hover-3 col-lg-6" style="background: url(<?= the_field('imagem'); ?>);"><div class="filtro-amarelo"></div><div class="text"><span class="title"><?= the_title(); ?> </span><!-- <span class="situacao"><?= the_field('situacao'); ?></span> --> <span class="arquitetura">Arquitetura: <?= the_field('arquitetura'); ?></span><span class="area">Área Construída: <?= the_field('construcao'); ?>m²</span> <a href="<?= the_permalink(); ?>" class="btn-cta">conheça +</a></div></div> <?php elseif ($contador == 4) : ?> </div></div> <?php endif; ?> <?php $contador++;
                endwhile;
            endif; ?> </div></div></div>